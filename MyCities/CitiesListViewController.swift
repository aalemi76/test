//
//  SecondViewController.swift
//  MyCities
//
//  Created by Catalina on 4/25/20.
//  Copyright © 2020 Deep Minds. All rights reserved.
//

import UIKit
import CoreData

class CitiesListViewController: UITableViewController, CurrentCityViewControllerDelegate {

    var citiesList = [City]()
    
    override func viewDidLoad() {
        loadListCities()
        super.viewDidLoad()
    }
    
    func CurrentCityViewController(_ controller: CurrentCityViewController, didAddCity city: City) {
        let newRowIndex = citiesList.count
        citiesList.append(city)
        let indexPath = IndexPath(row: newRowIndex, section: 0)
        let indexPaths = [indexPath]
        tableView.insertRows(at: indexPaths, with: .automatic)
        saveListCities()
    }
    //MARK:- Data Persistency
    func documentsDirectory() -> URL {
        let paths = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)
        return paths[0]
    }
    
    func dataFilePath() -> URL {
        return documentsDirectory().appendingPathComponent("Cities")
    }
    
    func saveListCities() {
        let encoder = PropertyListEncoder()
        do {
            let data = try encoder.encode(citiesList)
            try data.write(to: dataFilePath(), options: Data.WritingOptions.atomic)
        } catch {
            print("Error encoding cities list")
        }
    }
    
    func loadListCities(){
        let path = dataFilePath()
        if let data = try? Data(contentsOf: path){
            let decoder = PropertyListDecoder()
            do {
                citiesList = try decoder.decode([City].self, from: data)
            } catch {
                print("Error Decoding list")
            }
        }
    }
    //MARK:- Table View Delegates
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return citiesList.count
    }
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 60
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "City", for: indexPath)
        let city = citiesList[indexPath.row]
        cell.textLabel?.text = city.name
        cell.detailTextLabel?.text = city.time
        return cell
    }
    
    override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            let alert = UIAlertController(title: "Delete City?", message: "If you delete the item you no longer have access to it", preferredStyle: .alert)
            let cancelAction = UIAlertAction(title: "Cancel", style: .cancel, handler: nil)
            alert.addAction(cancelAction)
            let deleteAction = UIAlertAction(title: "Delete", style: .destructive){ _ in
                tableView.deleteRows(at: [indexPath], with: .fade)
                self.citiesList.remove(at: indexPath.row)
                self.saveListCities()
            }
            alert.addAction(deleteAction)
            present(alert, animated: true, completion: nil)
        }
    }
}
