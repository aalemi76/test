//
//  City.swift
//  MyCities
//
//  Created by Catalina on 4/26/20.
//  Copyright © 2020 Deep Minds. All rights reserved.
//

import UIKit

class City: NSObject, Codable{
    var name: String
    var time: String
    init(name: String, time: String) {
        self.name = name
        self.time = time
    }
}
