//
//  FirstViewController.swift
//  MyCities
//
//  Created by Catalina on 4/25/20.
//  Copyright © 2020 Deep Minds. All rights reserved.
//

import UIKit
import CoreData

protocol CurrentCityViewControllerDelegate: class {
    func CurrentCityViewController(_ controller: CurrentCityViewController, didAddCity city: City)
}

class CurrentCityViewController: UIViewController {
    
    @IBOutlet weak var cityTextField: UITextField!
    @IBOutlet weak var cityTimeLabel: UILabel!
    @IBOutlet weak var addCityButton: UIButton!
    
    weak var delegate: CurrentCityViewControllerDelegate?
    
    var list = [City]()

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
    }
    
    @IBAction func textFieldTapped(){
        cityTextField.resignFirstResponder()
        performSegue(withIdentifier: "ShowSearch", sender: cityTextField)
    }
    
    @IBAction func userDidPickedCity(_ segue: UIStoryboardSegue){
        let controller = segue.source as! SearchCityViewController
        cityTextField.text = controller.cityName
        cityTimeLabel.text = controller.cityTime
    }
    
    /*@IBAction func addCity() {
        let cityName = cityTextField.text ?? "No name founded"
        let cityTime = cityTimeLabel.text ?? ""
        let temp = City(name: cityName, time: cityTime)
        delegate!.CurrentCityViewController(self, didAddCity: temp)
    }*/
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        guard segue.identifier == "ShowList" else {return}
        let controller = segue.destination as! CitiesListViewController
        list = controller.citiesList
        let cityName = cityTextField.text ?? "No name fo"
        let cityTime = cityTimeLabel.text ?? ""
        let temp = City(name: cityName, time: cityTime)
        list.append(temp)
        controller.citiesList = list
        controller.saveListCities()
        controller.loadListCities()
    }
}

