//
//  File.swift
//  MyCities
//
//  Created by Catalina on 4/25/20.
//  Copyright © 2020 Deep Minds. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON
import Alamofire_SwiftyJSON
import CoreLocation

class SearchCityViewController: UITableViewController {
 
    @IBOutlet weak var searchBar: UISearchBar!
    
    var cityName: String?
    var cityTime: String?
    
    //To control and toggle search states
    var searchIsActive : Bool = false
    //Array to store the places returned in response
    var searchedPlaces: NSMutableArray = []
    //To decod data returned by API
    let decoder = JSONDecoder()
    
    let dateFormatter: DateFormatter = {
        let df = DateFormatter()
        df.timeStyle = .short
        return df
    }()
    
    static var mapBoxAPI = "https://api.mapbox.com/geocoding/v5/mapbox.places/"
    static var accessToken = "pk.eyJ1IjoiYWFsZW1pNzYiLCJhIjoiY2s5ZnltcGpzMGc1ejNtdGIyanZ5OHVpaSJ9.YSgU1GbrpvtfmehTyIxhhg"
    
    override func viewDidLoad() {
        searchBar.searchBarStyle = UISearchBar.Style.prominent
        //searchBar.tintColor = UIColor(red: 31, green: 33, blue: 36, alpha: 1)
        //searchBar.barTintColor = UIColor(white: 1, alpha: 0.3)
        searchBar.delegate = self
        searchBar.placeholder = "Enter City Name"
        navigationController?.title = "Search Bar"
        super.viewDidLoad()
    }
    
    func format(from date: Date) -> String {
        return dateFormatter.string(from: date)
    }
        
        //MARK:- Search City
        
        @objc func searchCity(query: String) {
            let urlAddress = "\(SearchCityViewController.mapBoxAPI)\(query).json?access_token=\(SearchCityViewController.accessToken)"
            Alamofire.request(urlAddress, method:  .get, parameters: nil, encoding: URLEncoding.default, headers: nil).responseSwiftyJSON { (dataResponse) in
                if dataResponse.result.isSuccess {
                    let result = JSON(dataResponse.result.value!)
                    guard let myResult = result["features"].array else {return}
                    for location in myResult {
                        try? print(location.rawData())
                        do {
                            let city = try self.decoder.decode(CityFeature.self, from: location.rawData())
                            self.searchedPlaces.add(city)
                            self.tableView.reloadData()
                        } catch {
                            guard let error = error as? DecodingError else {return}
                            print(error.localizedDescription)
                        }
                    }
                } else {
                    print(dataResponse.result.error ?? "No Error")
                }
            }
        }
    
    //MARK:- UITableView Delegate
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return searchedPlaces.count
    }
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 60
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "CityCell", for: indexPath)
        let predictedCity = searchedPlaces.object(at: indexPath.row) as! CityFeature
        let coordinate = predictedCity.geometry.coordinates
        let location = CLLocation(latitude: coordinate[0], longitude: coordinate[1])
        cell.textLabel?.text = predictedCity.text
        cell.detailTextLabel?.text = format(from: location.timestamp)
        return cell
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        guard let cell = tableView.cellForRow(at: indexPath) else {return}
        cityName = cell.textLabel?.text
        cityTime = cell.detailTextLabel?.text
    }
    
    // MARK:- Segue Settings
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "CityPicked"{
            let cell = sender as! UITableViewCell
            cityName = cell.textLabel?.text
            cityTime = cell.detailTextLabel?.text
        }
    }
}

//MARK:- UI Search Bar Delegate
extension SearchCityViewController: UISearchBarDelegate {
    
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        cancelSearching()
    }
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        view.endEditing(true)
    }
    
    func searchBarTextDidBeginEditing(_ searchBar: UISearchBar) {
        searchBar.setShowsCancelButton(true, animated: true)
    }
    
    func searchBarTextDidEndEditing(_ searchBar: UISearchBar) {
        searchBar.setShowsCancelButton(false, animated: false)
    }
    
    
    func cancelSearching(){
        searchIsActive = false
        searchBar.resignFirstResponder()
        searchBar.text = ""
        searchedPlaces.removeAllObjects()
        tableView.reloadData()
    }
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        NSObject.cancelPreviousPerformRequests(withTarget: self, selector: #selector(searchMe), object: nil)
        self.perform(#selector(searchMe), with: nil, afterDelay: 0.5)
        print(searchedPlaces)
        if searchBar.text != nil {
            searchIsActive = true
        } else {
            searchIsActive = false
        }
    }
    
    @objc func searchMe(){
        guard let cityName = searchBar.text else {return}
        searchCity(query: cityName)
    }
}
