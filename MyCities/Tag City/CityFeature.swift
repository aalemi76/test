//
//  City.swift
//  MyCities
//
//  Created by Catalina on 4/26/20.
//  Copyright © 2020 Deep Minds. All rights reserved.
//

import UIKit

struct CityFeature: Codable {
    var id: String!
    var type: String?
    var text: String
    var matchingPlaceName: String?
    var PlaceName: String?
    var geometry: Geometry
    var center: [Double]
    var properties: Properties
}

struct Geometry: Codable {
    var type: String?
    var coordinates: [Double]
}

struct Properties: Codable {
    var address: String?
}
